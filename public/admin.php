<?php

function theme_settings_page()
{

    echo '<div class="wrap">';
    echo '<h1>Companies House</h1>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("section");
    do_settings_sections("theme-options");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function display_api_key()
{
    echo '<input type="text" name="ch_search_api_key" id="ch_search_api_key" value="' . get_option('ch_search_api_key') . '" size="50"/>';
}

function display_theme_panel_fields()
{
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("ch_search_api_key", "API key", "display_api_key", "theme-options", "section");

    register_setting("section", "ch_search_api_key");
}

add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
    add_menu_page("", "Companies House", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

