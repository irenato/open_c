<?php

/**
 * @param $val
 * @param int $index
 * @return mixed
 */

function getResults($val, $index = 0)
{
    $search_param = str_replace(' ', '+', trim($val));
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/search/companies?q=' . $search_param . '&&start_index=' . (((int)trim($index)) * 20) . '&&items_per_page=20');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    return json_decode($response);
}

/**
 * @param $id
 * @param string $type
 * filing-history, officers, persons-with-significant-control
 * @return mixed
 */
function searchCompanyDataById($id, $type = '')
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, get_option('ch_search_api_key'));
    curl_setopt($curl, CURLOPT_URL, 'https://api.companieshouse.gov.uk/company/' . $id . '/' . $type);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
    if ($errno = curl_errno($curl)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    curl_close($curl);

    return json_decode($response);
}

/**
 * @param $date
 * @return string
 */
function formattingDate($date)
{
    if ($date) {
        return implode('.', array_reverse(explode('-', $date)));
    }
}

/**
 * @param $code_sic
 * @return mixed
 */
function searchDataByCode($code_sic)
{
    $handle = fopen(__DIR__ . "/SIC07.csv", "r");
    while (($data = fgetcsv($handle, 2000, ',')) !== FALSE) {
        if ((int)$data[0] == (int)$code_sic) {
            return $data[1];
            fclose($handle);
        }
    }
}
