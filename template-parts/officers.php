<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.10.17
 * Time: 20:22
 */

?>

<div class="tab-pane" id="block-3">
    <div class='officers'>
        <?php if ($company->officers): ?>
            <h3 class="title"><?= count($company->officers) ?> найдено</h3>
            <?php foreach ($company->officers as $officer) :
                $item = $officer->officer; ?>
                <div class="officer">
                    <h4 class='title'>Имя агента</h4>
                    <h2 class="title"><?= $item->name ?></h2>
                        <?php if (!$item->inactive): ?>
                        <span class="status active">ACTIVE</span></h2>
                    <?php else: ?>
                        <span class="status resigned">RESIGNED</span></h2>
                    <?php endif; ?>
                    <h4 class='title'>Адрес агента</h4>
                    <?php if ($item->address) : ?>
                        <p class="title">
                            <?= $item->address; ?>
                        </p>
                    <?php endif; ?>

                    <ul class='description'>

                        <?php if ($item->position) : ?>
                            <li>
                                <h4 class="title">Директора/офицеры
                                </h4>
                                <p class="title"> <?= $item->name . ', ' . str_replace('-', ' ', $item->position) ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->start_date) : ?>
                            <li>
                                <h4 class="title">Назначен</h4>
                                <p class="title"> <?= formattingDateOC($item->start_date) ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->date_of_birth) : ?>
                            <li>
                                <h4 class="title">Дата рождения</h4>
                                <p class="title"> <?= formattingDateOC($item->date_of_birth) ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->end_date) : ?>
                            <li>
                                <h4 class="title">Подал в отставку</h4>
                                <p class="title"> <?= formattingDateOC($item->end_date) ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->identification) : ?>

                            <?php if ($item->identification->identification_type) : ?>
                                <?php if ($item->identification->identification_type == 'non-eea') : ?>
                                    <h5 class="title">Зарегистрирован не в Европейской экономической
                                        зоне</h5>
                                    <br>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php if ($item->identification->legal_authority) : ?>
                                <li>
                                    <h4 class="title">Регулируемый правовыми нормами</h4>
                                    <p class="title"> <?= $item->identification->legal_authority ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if ($item->identification->legal_form) : ?>
                                <li>
                                    <h4 class="title">Юридическая форма</h4>
                                    <p class="title"> <?= $item->identification->legal_form ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if ($item->identification->place_registered) : ?>
                                <li>
                                    <h4 class="title">Место регистрации</h4>
                                    <p class="title"> <?= $item->identification->place_registered ?></p>
                                </li>
                            <?php endif; ?>

                            <?php if ($item->identification->registration_number) : ?>
                                <li>
                                    <h4 class="title">Регистрационный номер</h4>
                                    <p class="title"> <?= $item->identification->registration_number ?></p>
                                </li>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if ($item->nationality) : ?>
                            <li>
                                <h4 class="title">Национальность</h4>
                                <p class="title"> <?= $item->nationality ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->country_of_residence) : ?>
                            <li>
                                <h4 class="title">Страна проживания</h4>
                                <p class="title"> <?= $item->country_of_residence ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->occupation) : ?>
                            <li>
                                <h4 class="title">Профессия</h4>
                                <p class="title"> <?= $item->occupation ?></p>
                            </li>
                        <?php endif; ?>

                        <?php if ($item->natures_of_control) : ?>
                            <li>
                                <h4 class="title">Характер управления</h4>
                                <?php foreach ($item->natures_of_control as $value) ?>
                                <p class="people-nc title"> <?= str_replace('-', ' ', $value) ?></p>
                            </li>
                        <?php endif; ?>
                    </ul>

                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
        <?php endif; ?>
    </div>
</div>
