<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.10.17
 * Time: 20:22
 */

?>

<div class="tab-pane" id="block-2">
    <div class='officers'>
        <h3 class="title"><?= count($company->trademark_registrations) ?> найдено</h3>
        <?php foreach ($company->trademark_registrations as $trademark_registration) :
            $item = $trademark_registration->trademark_registration;
            ?>
            <div class="officer">
                <h4><?= $item->register_name ?></h4>
                <?php if ($item->mark_details->mark_text) : ?>
                    <h4 class="title"><?= $item->mark_details->mark_text ?></h4>
                <?php endif; ?>
                <?php if ($item->mark_details->mark_image_url) : ?>
                    <img src="<?= $item->mark_details->mark_image_url ?>"/>
                <?php endif; ?>
                <ul class='description'>
                    <?php if ($item->registration_date) : ?>
                        <li>
                            <h4 class="title">Зарегистрирован</h4>
                            <p class="title"> <?= formattingDateOC($item->registration_date) ?></p>
                        </li>
                    <?php endif; ?>

                    <?php if ($item->expiry_date) : ?>
                        <li>
                            <h4 class="title">Закрыт</h4>
                            <p class="title"> <?= formattingDateOC($item->expiry_date) ?></p>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endforeach; ?>
    </div>
</div>
