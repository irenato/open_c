<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 28.10.17
 * Time: 20:25
 */

if ($data->results->total_count > 0) : ?>

    <div class="search-results-list">
        <h2>Результаты поиска</h2>
        <?php $pages = floor($data->total_results / 20) ?>
        <h3>Найдено: <?= $data->results->total_count ?> </h3>
        <ul>
            <?php
            $i = -1;
            foreach ($data->results->companies as $item) : ?>
                <li <?= (++$i == 0 && !key_exists('oc-page', $_GET)) ? "class=first-item" : '' ?>>
                    <a href="<?= createUrl($item) ?>"
                       class='search-result-item'>
                        <?= $item->company->name . ($item->company->registered_address && $item->company->registered_address->country ? ' (' . $item->company->registered_address->country . ') ' : '') ?>
                    </a>
                    <?php if ($item->company->inactive): ?>
                        <span class="status resigned">inactive</span>
                    <?php endif; ?>
                    <?php if ($item->company->company_type) : ?>
                        <p class="title"><?= $item->company->company_type ?></p>
                    <?php endif; ?>
                    <?php if ($item->company->registered_address_in_full) : ?>
                        <p class="title">
                            <?= $item->company->registered_address_in_full ?>
                        </p>
                    <?php endif; ?>
                </li>

            <?php endforeach; ?>
        </ul>

        <!--            pagination (start) -->
        <?php if ((int)$data->results->total_pages > 1) : ?>
            <ul class="pagination">

                <?php if (isset($_GET['oc-page']) && $_GET['oc-page'] > 1) : ?>
                    <li class="arrow-left"><a
                                href="<?= '?val=' . $_GET['val'] . '&oc-page=' . ((int)$_GET['oc-page'] - 1) ?>"></a>
                    </li>
                <?php endif; ?>

                <?php
                $counter = createCounter($data->results->total_pages);
                for ($i = 1; $i <= (int)$data->results->total_pages; $i += $counter) : ?>
                    <?php $current_page = isset($_GET['oc-page']) ? $_GET['oc-page'] : 1; ?>
                    <li class="<?= ($current_page == $i) ? 'active' : '' ?>"><a
                                href="<?= '?val=' . $_GET['val'] . '&oc-page=' . $i ?>"><?= $i ?></a>
                    </li>
                <?php endfor; ?>

                <?php if ((int)$_GET['oc-page'] < (int)$data->results->total_pages) : ?>
                    <li class="arrow-right"><a
                                href="<?= '?val=' . $_GET['val'] . '&oc-page=' . ((int)$_GET['oc-page'] + 1) ?>"></a>
                    </li>
                <?php endif; ?>

            </ul>

            <div class="col-xs-12">
                <form class="form-horizontal pages" method="get" action="#">
                    <input type="hidden" name="val" class="form-control"
                           value="<?= isset($_GET['val']) ? $_GET['val'] : '' ?>">
                    <input type="search" name="oc-page" class="form-control"
                           placeholder="Введите номер страницы: (1 - <?= (int)$data->results->total_pages; ?>)">
                    <input type="submit" value="Перейти">
                </form>
            </div>

        <?php endif; ?>

    </div>
<?php else: ?>
    <h2 id="not-items-found">Отсутствует информация по Вашему запросу</h2>
<?php endif; ?>