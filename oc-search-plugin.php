<?php
/*
Plugin Name: Plugin for OpenCorporates
Version: 2.0
Description: Search Plugin for OpenCorporates. Add the shortcode to the page content ([oc_search])
Author: Alscon (Khalimov Renato)
*/

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

include_once(dirname(__FILE__) . '/OpenCorporates.php');
include_once(dirname(__FILE__) . '/public/admin.php');
include_once(dirname(__FILE__) . '/public/results.php');

register_activation_hook(__FILE__, array( 'OpenCorporates', 'on_activation' ) );
register_deactivation_hook(__FILE__, array( 'OpenCorporates', 'on_deactivation' ));

OpenCorporates::init();
